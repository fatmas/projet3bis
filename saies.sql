use mea;
CREATE TABLE IF NOT EXISTS customers( 
   customer_id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(40) NOT NULL,
   last_name VARCHAR(40)NOT NULL,
   phone  VARCHAR(25) NOT NULL,
   email VARCHAR(40)NOT NULL,
   street VARCHAR(40),
   city VARCHAR(50),
   state VARCHAR(25),
   zip_code CHAR(5)

);

CREATE TABLE IF NOT EXISTS orders( 
   order_id INT AUTO_INCREMENT PRIMARY KEY,
   customer_id INT NOT NULL,
   order_status TINYINT NOT NULL ,
   required_date  DATE NOT NULL,
   shipped_date DATE NOT NULL,
   store_id INT,
   staff_id INT,
   FOREIGN KEY(customer_id) REFERENCES customers(customer_id),
   FOREIGN KEY(staff_id) REFERENCES staffs(staff_id),
   FOREIGN KEY(store_id) REFERENCES stores(store_id)
);

CREATE TABLE IF NOT EXISTS staffs( 
   staff_id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(40) NOT NULL,
   last_name VARCHAR(40) NOT NULL,
   email VARCHAR (40) NOT NULL ,
   phone   VARCHAR(25) NOT NULL,
   active TINYINT NOT NULL,
   store_id INT NOT NULL,
   manager_id INT NOT NULL,
   FOREIGN KEY(store_id) REFERENCES stores(store_id),
   FOREIGN KEY(manager_id) REFERENCES staffs(staff_id)
);

CREATE TABLE IF NOT EXISTS stores( 
   store_id INT AUTO_INCREMENT PRIMARY KEY,
   store_name VARCHAR(255) NOT NULL,
   phone VARCHAR(25) NOT NULL,
   email VARCHAR (40) NOT NULL ,
   street   VARCHAR(40) NOT NULL,
   city VARCHAR(40) NOT NULL,
   state VARCHAR(40) NOT NULL,
   zip_id CHAR(5)
);

CREATE TABLE IF NOT EXISTS order_items( 
   order_id INT ,
   item_id INT ,
   product_id INT NOT NULL,
   quantity INT NOT NULL ,
   list_price   DECIMAL(10, 2) NOT NULL,
   discount DECIMAL (4, 2) NOT NULL DEFAULT 0,
   PRIMARY KEY (order_id, item_id),
   FOREIGN KEY(order_id) REFERENCES orders(order_id),
   FOREIGN KEY(product_id) REFERENCES products(product_id)
);
CREATE TABLE categories (
	category_id INT AUTO_INCREMENT  PRIMARY KEY,
	category_name VARCHAR (255) NOT NULL
);

CREATE TABLE brands (
	brand_id INT AUTO_INCREMENT  PRIMARY KEY,
	brand_name VARCHAR (255) NOT NULL
);

CREATE TABLE products (
	product_id INT AUTO_INCREMENT  PRIMARY KEY,
	product_name VARCHAR (255) NOT NULL,
	brand_id INT NOT NULL,
	category_id INT NOT NULL,
	model_year SMALLINT NOT NULL,
	list_price DECIMAL (10, 2) NOT NULL,
	FOREIGN KEY (category_id) REFERENCES categories (category_id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (brand_id) REFERENCES brands (brand_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE stocks (
	store_id INT,
	product_id INT,
	quantity INT,
	PRIMARY KEY (store_id, product_id),
 	FOREIGN KEY (product_id) REFERENCES products (product_id) ,
	FOREIGN KEY (store_id) REFERENCES stores (store_id)
);

